program Soal_5;
	uses crt;
	var tinggi, merosot, jml_naik:Real;
	var hari, naik:Integer;
	var conf:Char;

begin
	repeat
		clrscr();
			
		write('Masukan Tinggi Gunung Pasir = ');readln(tinggi);

		writeln('-----------------------------------------------------------');
	    writeln('Jumlah Hari    Naik    Merosot    Jumlah Naik    Ketinggian');
	    writeln('                                    Per Hari       Gunung  ');
	    writeln('-----------------------------------------------------------');

		// deklarasi variable
		hari := 1;
		naik := 3;
		merosot := 1;

		repeat
			if hari > 15 then 
				begin
					naik := 2;
					merosot := 1.50;
				end
			else if hari > 5 then
				begin
					naik := 2;
					merosot := 1.25;
				end;
			
			jml_naik := naik-merosot;
			tinggi := tinggi-jml_naik;
			
			writeln(hari:7,naik:10,merosot:12:2,jml_naik:12:2,tinggi:14:2);
			
			hari := hari+1;
		until tinggi <= 0;
		
		write('Ulang (Y/N): ');readln(conf);
	until lowercase(conf) = 'n';
	readln();
end.