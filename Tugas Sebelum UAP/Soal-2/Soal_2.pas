program Soal_2;
	uses crt;
	var nama, jenis_kendaraan:String;
	var lama_pinjam:Integer;
	var harga_sewa, ttl_bayar, bayar, kembalian:LongInt;
	var conf:Char;
begin
	repeat
		
	clrscr();

	writeln('====== Program Sewa Kendaraan ======');
	writeln('====================================');

	write('Masukan Nama Anda: ');readln(nama);
	write('Masukan Jenis Kendaraan (mt/mb): ');readln(jenis_kendaraan);
	write('Masukan Lama Pinjam: ');readln(lama_pinjam);
	
	if (lowercase(jenis_kendaraan) = 'mt') then 
		begin
			harga_sewa := 300000;
			if (lama_pinjam > 6) then 
				begin
					ttl_bayar := ( harga_sewa * lama_pinjam ) - ( ( (harga_sewa * lama_pinjam) * 5 ) DIV 100 );
				end
			else 
				begin
					ttl_bayar := harga_sewa * lama_pinjam;
				end
		end
	else if (lowercase(jenis_kendaraan) = 'mb') then 
		begin
			harga_sewa := 500000;
			if (lama_pinjam > 6) then 
				begin
					ttl_bayar := ( harga_sewa * lama_pinjam ) - ( ( (harga_sewa * lama_pinjam) * 8 ) DIV 100 );
				end
			else 
				begin
					ttl_bayar := harga_sewa * lama_pinjam;
				end
		end
	else
		begin
			harga_sewa := 0;
		end;

	writeln('====================================');

	if harga_sewa=0 then
		begin
			writeln('Jenis Kendaraan Tidak Sesuai'); 	
		 end
	else 
		begin
			repeat
				
			writeln('Haloo ',nama,' Total yang harus anda bayar adalah: ',ttl_bayar); 	
			write('Uang yang anda bayarkan adalah: ');readln(bayar);

			if (ttl_bayar > bayar) then 
				begin
					writeln();
					writeln('Uang yang anda bayarkan masih kurang!');
					writeln('====================================');
					writeln();writeln();
				end;

			until ttl_bayar <= bayar;

			kembalian := bayar - ttl_bayar;
			writeln('kembaliannya: ',kembalian);
		end;

	writeln();writeln();
	write('Apakah anda ingin mencobanya kembali (y/n)? ');readln(conf);

	until lowercase(conf) = 'n';
end.