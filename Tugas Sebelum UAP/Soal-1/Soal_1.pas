program Soal_1;
	uses crt;
	var nama:String;
	var tb, npd, npu, nak:Integer;
	var conf:Char;
begin
	repeat
		
		clrscr();

		writeln('====== Program Seleksi Karyawan Security ======');
		writeln('===============================================');
		
		write('Masukan Nama Anda: ');readln(nama);
		write('Masukan Tinggi Badan: ');readln(tb);
		write('Masukan Nilai Pengetahuan Dasar: ');readln(npd);
		write('Masukan Nilai Pengetahuan Umum: ');readln(npu);
		write('Masukan Nilai Akademik: ');readln(nak);

		writeln('===============================================');

		if ((tb > 180) AND (npd > 60) AND (npu > 70) AND (nak > 80)) then 
			begin
				writeln('Selamat ',nama,' Anda Diterima');
			end
		else 
			begin
				writeln('Anda Tidak Diterima.');
			end;

		writeln();writeln();
		write('Apakah anda ingin mencobanya kembali (y/n)? ');readln(conf);

	until lowercase(conf) = 'n';
	readln();
end.