program Soal_4;
	uses crt;
	var counter, gaji, average, ttl:Integer;
	var cKaryawan: array[0..4] of integer;
	var conf:String;
begin
	repeat

		clrscr();

		//input gaji
		counter := 0;
		while (counter < 5) do
			begin
				write('Masukan Gaji Karyawan ke-',(counter+1),': ');
				ReadLn(gaji);

				cKaryawan[counter] := gaji;
				counter := counter+1;
			end;

		WriteLn();WriteLn();

		//cetak gaji karyawan
		counter := 0;
		ttl := 0;
		while (counter < 5) do
			begin
				write('Gaji Karyawan ke-',(counter+1),': ');
				WriteLn(cKaryawan[counter]);

				ttl := ttl+cKaryawan[counter];

				counter := counter+1;
			end;

		WriteLn();WriteLn();

		WriteLn('Total gaji karyawan: ',ttl);
		average := ttl div 5;
		WriteLn('Rata - rata gaji karyawan: ',average);

		writeln();writeln();
		write('Apakah anda ingin mencobanya kembali (y/n)? ');readln(conf);

	until lowercase(conf) = 'n';
	readln();
end.