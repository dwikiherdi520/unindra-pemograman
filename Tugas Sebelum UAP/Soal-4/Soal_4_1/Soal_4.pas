program Soal_4;
	uses crt;
	var jmlKaryawan, counter, gaji, average, ttl:Integer;
	var cKaryawan: array of integer;
	var conf:String;
begin
	repeat

		clrscr();
		Write('Masukan jumlah karyawan: ');
		ReadLn(jmlKaryawan);

		//set panjang array
		SetLength(cKaryawan,jmlKaryawan);

		//input gaji
		counter := 0;
		while (counter < jmlKaryawan) do
			begin
				write('Masukan Gaji Karyawan ke-',(counter+1),': ');
				ReadLn(gaji);

				cKaryawan[counter] := gaji;
				counter := counter+1;
			end;

		WriteLn();WriteLn();

		//cetak gaji karyawan
		counter := 0;
		ttl := 0;
		while (counter < jmlKaryawan) do
			begin
				write('Gaji Karyawan ke-',(counter+1),': ');
				WriteLn(cKaryawan[counter]);

				ttl := ttl+cKaryawan[counter];

				counter := counter+1;
			end;

		WriteLn();WriteLn();

		WriteLn('Total gaji karyawan: ',ttl);
		average := ttl div jmlKaryawan;
		WriteLn('Rata - rata gaji karyawan: ',average);

		writeln();writeln();
		write('Apakah anda ingin mencobanya kembali (y/n)? ');readln(conf);

	until lowercase(conf) = 'n';
	readln();
end.