program Soal_3;
	uses crt;
	var jenis_kelamin, conf:Char;
	var npm:Integer;
begin
	repeat
		
	clrscr();
	write('Masukan Jenis Kelamin (L/P) = ');readln(jenis_kelamin);
	jenis_kelamin := upcase(jenis_kelamin);

	case jenis_kelamin of
		'L': 
			begin
				writeln('TETAP DIDALAM KELAS');
				write('Masukan NPM = ');readln(npm);

				if (npm MOD 2 = 0) then 
					begin
						writeln('Kerjakan Soal A');
					end
				else
					begin
						writeln('Kerjakan Soal B');
					end;
			end;
		'P':
			begin
				writeln('KELUAR KELAS');
			end;
		else
			begin
				writeln('PULANG');
			end;
	end;

	writeln();
	write('Ulang (Y/T) = ');readln(conf);

	until upcase(conf) = 'T';

	readln();
end.