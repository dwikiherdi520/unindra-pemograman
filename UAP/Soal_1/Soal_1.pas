program Soal_1;
	uses crt;
	var nama, nama_jenis,ukuran_katun,is_ukuran:String;
	var jenis_celana, ukuran_jeans, harga, jml_beli, ttl_harga, ttl_disc, disc:Integer;
begin
	clrscr();
	writeln('':5,'Harga Celana Bedasarkan Jenisnya','':5);
	writeln('==========================================');

	writeln('1. Celana Jeans');
	writeln('a. Ukuran':5,' 27 s/d 30 = Rp 150.000,-');
	writeln('b. Ukuran':5,' 31 s/d 34 = Rp 175.000,-');
	writeln('c. Ukuran':5,' 35 s/d 38 = Rp 200.000,-');
	writeln();
	writeln('2. Celana Katun');
	writeln('a. Ukuran M':5,' = Rp 225.000,-');
	writeln('b. Ukuran L':5,' = Rp 250.000,-');
	writeln('c. Ukuran XL':5,' = Rp 300.000,-');
	writeln();

	write('Masukan Nama Anda: ');readln(nama);
	write('Masukan Pilihan Celana (1 atau 2): ');readln(jenis_celana);

	if (jenis_celana=1) then
		begin
			nama_jenis := 'Celana Jeans';
			write('Masukan Ukuran Celana (27 s/d 38): ');readln(ukuran_jeans);
			str(ukuran_jeans, is_ukuran);
			case ukuran_jeans of
				27..30: harga := 150000;
				31..34: harga := 175000;
				35..38: harga := 200000;
				else harga := 0;
			end
		end 
	else if (jenis_celana=2) then
		begin
			nama_jenis := 'Celana Katun'; 	
			write('Masukan Ukuran Celana (M/L/XL): ');readln(ukuran_katun);
			ukuran_katun := upcase(ukuran_katun);
			is_ukuran := ukuran_katun;
			case ukuran_katun of
				'M': harga := 225000;
				'L': harga := 250000;
				'XL': harga := 300000;
				else harga := 0;
			end
		end
	else 
		begin
			nama_jenis := '';
		end;
	writeln(); writeln();
	writeln('Anda Memilih ',nama_jenis);
	writeln('Harga Celana Anda Ukuran ',is_ukuran,' = Rp ',harga);
	writeln(); writeln();
	write('Masukan Jumlah Beli: ');readln(jml_beli);
	ttl_harga := harga * jml_beli;
	if ttl_harga < 500000 then 
		begin
			disc := 2;
		end
	else
		begin
			disc := 4;
		end;
	ttl_disc := ttl_harga - (ttl_harga * disc div 100);
	writeln(); writeln();
	writeln('Total Bayar (Sebelum Diskon): Rp ',ttl_harga);
	writeln('Total Bayar (Setelah Diskon ',disc,'%): Rp ',ttl_disc);
	readln();
end.