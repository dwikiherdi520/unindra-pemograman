program Soal_3;
	uses crt;
	var data,jml,i,j:Integer;
	var arr: array of integer;
begin
	clrscr();
	writeln('=== Program Data Konsumen ===');
	writeln('=============================');

	write('Masukan banyak data: ');readln(jml);
	setlength(arr,jml);

	for i := 1 to jml do 
		begin
			write('Pendapatan penjualan Data bulan ke',i,': ');readln(data);
			arr[i] := data;
		end;

	writeln('=============================');
	
	for j := 1 to jml do 
		begin
			writeln('| Bulan ke ',j,' yaitu ',arr[j],' konsumen |');
		end;
	readln();
end.